package week02.di;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import week01.models.Week1Person;

public class AppMain {

	public static void main(String[] args) throws ParseException {
		ApplicationContext context = new AnnotationConfigApplicationContext(Configure.class);
		// parameter 없이 객체 호출 됨 
		Week1Person itemFromContext = context.getBean(Week1Person.class);
		System.out.println(itemFromContext);
		
		Configure configure = context.getBean(Configure.class);
		SimpleDateFormat formatter = configure.formatter();
		Week1Person item1 = configure.week1Person(formatter);
		Week1Person item2 = configure.week1Person(formatter);
		
		System.out.println("is == [" + (item1 == item2) + "]");
		System.out.println("item1[" + item1 + "]");
		System.out.println("item2[" + item2 + "]");
		
		
		System.out.println("\n\n");
		// injection 된 객체 
		SampleComponent component = context.getBean(SampleComponent.class);
		// property injection
		System.out.println(component.toStringPerson());
	}
}
