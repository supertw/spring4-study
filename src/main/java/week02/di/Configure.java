package week02.di;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import week01.models.Sexual;
import week01.models.Week1Person;

@Configuration
@ComponentScan(basePackages="week02")
public class Configure {

	@Bean
	@Scope("singleton")
	public Week1Person week1Person(SimpleDateFormat formatter) throws ParseException {
		Date d = formatter.parse("2010-01-31");
		
		Week1Person item = new Week1Person();
		item.setHeight(100);
		item.setName("jk");
		item.setSex(Sexual.MAN);
		item.setBirthDate(d);
		return item;
	}

	@Bean
	public SimpleDateFormat formatter() {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter;
	}
		
}
