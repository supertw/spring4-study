package week02.di;

import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import week01.models.Week1Person;

@Component
public class SampleComponent {

	@Autowired
	public SimpleDateFormat formatter;
	
	@Autowired
	public Week1Person person;
	
	public String toStringPerson() {
		return person.toString();
	}
}
