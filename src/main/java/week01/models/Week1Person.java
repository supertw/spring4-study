package week01.models;

import java.util.Date;

public class Week1Person {

	private int height;
	private String name;
	private Date birthDate;
	private Sexual sex;
	
	public Week1Person() {
	}

	public Week1Person(int height, String name, Date birthDate, Sexual sex) {
		this.height = height;
		this.name = name;
		this.birthDate = birthDate;
		this.sex = sex;
	}
	
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getBirthDate() {
		return birthDate;
	}
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}
	public Sexual getSex() {
		return sex;
	}
	public void setSex(Sexual sex) {
		this.sex = sex;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Week1Person) {
			Week1Person other = (Week1Person) obj;
			return this.height == other.height && this.name.equals(other.name) && this.sex == other.sex;
		} else {
			return false;
		}
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public String toString() {
		return "height[" + height + "]name[" + name + "]sex[" + sex + "]birthdate[" + birthDate.toGMTString() + "]hash[" + hashCode() + "]";
	}
	
	
}
