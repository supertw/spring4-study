package week01.quiz;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import week01.models.Week1Person;

public class AppMain {

	public static void main(String[] args) throws ParseException {
		ApplicationContext context = new AnnotationConfigApplicationContext(Configure.class);
		
		Configure configure = context.getBean(Configure.class);
		SimpleDateFormat formatter = configure.formatter();
		Week1Person item1 = configure.week1Person(formatter);
		Week1Person item2 = configure.week1Person(formatter);

		System.out.println("is == [" + (item1 == item2) + "]");
		System.out.println("item1[" + item1 + "]");
		System.out.println("item2[" + item2 + "]");

	}
	
}
