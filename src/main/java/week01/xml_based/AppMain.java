package week01.xml_based;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import week01.models.Sexual;
import week01.models.Week1Person;

public class AppMain {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws ParseException {
		ApplicationContext context = new ClassPathXmlApplicationContext("classpath:/week01/applicationContext.xml");
		Week1Person itemFromContext = context.getBean(Week1Person.class);

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date d = formatter.parse("2010-01-31");
		
		Week1Person itemFromNew = new Week1Person();
		itemFromNew.setHeight(100);
		itemFromNew.setName("jk");
		itemFromNew.setSex(Sexual.MAN);
		itemFromNew.setBirthDate(d);
		
		System.out.println("equal with ==[" + (itemFromContext == itemFromNew) + "]");
		System.out.println("equal with equal[" + itemFromContext.equals(itemFromNew) + "]");
		System.out.println("itemFromContext[" + itemFromContext + "]");
		System.out.println("itemFromNew[" + itemFromNew + "]");

		System.out.println("\n");
		
//		Week1Person nextItemFromContext = context.getBean("week01Person", Week1Person.class);
		Week1Person nextItemFromContext = (Week1Person) context.getBean("week01Person");
		
		System.out.println("equal with ==[" + (itemFromContext == nextItemFromContext) + "]");
		System.out.println("equal with equal[" + itemFromContext.equals(nextItemFromContext) + "]");
		System.out.println("itemFromContext[" + itemFromContext + "]");
		System.out.println("nextItemFromContext[" + nextItemFromContext + "]");
	}
}
