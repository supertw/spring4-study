package week03;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import week01.models.Week1Person;
import week03.db.DbServiceLayer;

@SpringBootApplication
@ComponentScan(basePackages="week03")
public class AppMain implements CommandLineRunner {

	public static void main(String[] args) throws ParseException {
		SpringApplication.run(AppMain.class, args);
	}

	@Autowired
	private ApplicationContext context;
	
	@Autowired
	private DbServiceLayer service;
	
	@Override
	public void run(String... args) throws Exception {
		// parameter 없이 객체 호출 됨 
		Week1Person itemFromContext = context.getBean(Week1Person.class);
		System.out.println(itemFromContext);
		
		Configure configure = context.getBean(Configure.class);
		SimpleDateFormat formatter = configure.formatter();
		Week1Person item1 = configure.week1Person(formatter);
		Week1Person item2 = configure.week1Person(formatter);
		
		System.out.println("is == [" + (item1 == item2) + "]");
		System.out.println("item1[" + item1 + "]");
		System.out.println("item2[" + item2 + "]");
		
		System.out.println("From DB select start");
		List<Week3Person> selectAll = service.selectAll();
		selectAll.stream().forEach(item -> System.out.println("item[" + item + "]"));
		System.out.println("From DB select End");
	}
	
	
	
}
