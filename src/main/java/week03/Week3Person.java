package week03;

import java.util.Date;

public class Week3Person {

	private int id;
	private String name;
	private Date birthDate;
	
	public Week3Person() {
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	@SuppressWarnings("deprecation")
	@Override
	public String toString() {
		return "id[" + id + "]name[" + name + "]birthdate[" + birthDate.toGMTString() + "]hash[" + hashCode() + "]";
	}
	
	
}
