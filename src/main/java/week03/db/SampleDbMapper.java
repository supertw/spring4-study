package week03.db;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import week03.Week3Person;

public interface SampleDbMapper {
	
	@Select("SELECT * FROM PERSON WHERE id = #{id} ")
	Week3Person getItem(@Param("id") int id);

	@Select("SELECT * FROM PERSON ")
	List<Week3Person> selectAll();
	
}
