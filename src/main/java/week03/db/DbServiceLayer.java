package week03.db;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import week03.Week3Person;

@Service
public class DbServiceLayer {

	@Autowired
	private SampleDbMapper dbMapper;
	
	public Week3Person getItem(int id) {
		return dbMapper.getItem(id);
	}
	
	public List<Week3Person> selectAll() {
		return dbMapper.selectAll();
	}
	
}
