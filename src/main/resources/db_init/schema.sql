DROP TABLE IF EXISTS PERSON;

CREATE TABLE PERSON
(
	id	INT primary key auto_increment,
	name VARCHAR(100),
	birthDate date
);

insert into PERSON (name, birthDate) values('name1', parseDateTime('20180101000000','yyyyMMddHHmmss')) ;
insert into PERSON (name, birthDate) values('name2', parseDateTime('20180102000000','yyyyMMddHHmmss')) ;
insert into PERSON (name, birthDate) values('name3', parseDateTime('20180103000000','yyyyMMddHHmmss')) ;
insert into PERSON (name, birthDate) values('name4', parseDateTime('20180104000000','yyyyMMddHHmmss')) ;